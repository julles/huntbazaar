# HUNTBAZAAR

## Requirements / Persyaratan

-   Composer
-   GIT
-   MYSQL 5.7 / 8
-   PHP >= 7.2.5
-   BCMath PHP Extension
-   Ctype PHP Extension
-   Fileinfo PHP Extension
-   JSON PHP Extension
-   Mbstring PHP Extension
-   OpenSSL PHP Extension
-   PDO PHP Extension
-   Tokenizer PHP Extension
-   XML PHP Extension

## Cara install

Clone repository

```sh
git clone https://julles@bitbucket.org/julles/huntbazaar.git huntbazaar && cd huntbazaar
```

Copy environtment example

```sh
    cp .env.example .env
```

Konfigurasi koneksi database, email driver atau yg lainnya pada .env file

```sh
...

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=huntbazaar
DB_USERNAME=root
DB_PASSWORD=root

MAIL_MAILER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=yourusername
MAIL_PASSWORD=yourpassword
MAIL_ENCRYPTION=tls
...

```

Jalankan composer untuk mendapatkan semua package depedencies yang dibutuhkan

```sh
composer install
```

Migrate database and seed the data

```sh
php artisan migrate && php artisan db:seed

```

Jalankan command dibawah ini untuk melisten semua queue yang tersedia atau anda bisa menambahkan command ini disupervisord diserver anda.

```sh
php artisan queue:work --tries=3

```

Tambahkan command dibawah ini discheduller server anda (crontab), untuk melisten semua invitation yang
sudah siap dikirim thankyou email.

```sh
* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1
```

atau anda bisa menjalankan manual, dengan command dibawah ini

```sh
php artisan email-thanks-daemon

```

Admin Account:

-   email: admin@huntbazaar.com
-   password: admin

By: [Muhamad Reza Abdul Rohim](https://github.com/julles)
