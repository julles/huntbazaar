<?php

use App\Models\InvitationStatus;
use Illuminate\Database\Seeder;

class InvitationStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                "id" => 1,
                "status" => "created",
            ],
            [
                "id" => 2,
                "status" => "User Submited",
            ],
        ];

        InvitationStatus::insert($data);
    }
}
