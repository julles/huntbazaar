<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invitations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("invitation_status_id");
            $table->string("email")->unique()->index();
            $table->string("name", 50)->nullable();
            $table->date("date_of_birth")->nullable();
            $table->enum("gender", ["m", "f"])->nullable()->default(null);
            $table->string("code", 20)->nullable()->index();
            $table->timestamps();

            $table->foreign("invitation_status_id")
                ->references("id")
                ->on("invitation_statuses")
                ->onUpdate("cascade")
                ->onDelete("restrict");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invitations');
    }
}
