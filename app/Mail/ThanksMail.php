<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ThanksMail extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct($model)
    {
        $this->model = $model;
    }

    public function build()
    {
        return $this->markdown('mails.thanks', [
            "model" => $this->model,
        ]);
    }
}
