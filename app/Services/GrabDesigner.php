<?php

namespace App\Services;

use KubAT\PhpSimple\HtmlDomParser;

class GrabDesigner
{
    private $url;

    public function __construct()
    {
        $this->url = "https://www.huntstreet.com/designer";
    }

    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    public function getData()
    {
        return cache()->remember("grab_designers", 10, function () {
            $html = file_get_contents($this->url);
            $dom = HtmlDomParser::str_get_html($html);
            $data = [];
            foreach ($dom->find("div.designerBox > div.dbRight > ul > li") as $a) {
                $onlyText = strip_tags($a->innertext);

                $data[$onlyText] = $onlyText;
            }

            return $data;
        });
    }
}
