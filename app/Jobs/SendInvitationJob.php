<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use App\Mail\Admin\InvitationMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendInvitationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $invitation;

    public function __construct($invitation)
    {
        $this->invitation = $invitation;
    }

    public function handle()
    {
        Mail::to($this->invitation->email)->send(new InvitationMail($this->invitation));
    }
}
