<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvitationStatus extends Model
{
    public $guarded = [];
}
