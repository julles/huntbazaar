<?php

namespace App\Models;

use App\Services\Helper;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    protected $guarded = [];

    public static function boot()
    {
        parent::boot();

        static::updating(function ($model) {
            $model->invitation_status_id = 2;
        });
    }

    public function getUrlAttribute()
    {
        $key = Helper::encryption($this->id);
        $id = $this->id;
        return url("invitation/{$id}/{$key}");
    }

    public function status()
    {
        return $this->belongsTo(InvitationStatus::class, "invitation_status_id");
    }

    public function getGenderTextAttribute()
    {
        $result = "";

        if (!empty($this->gender)) {
            $result = $this->gender == "f" ? "Female" : "Male";
        }

        return $result;
    }

    public function getDesignersTextAttribute()
    {
        $result = "";

        if (!empty($this->designers)) {
            $designersArray = json_decode($this->designers);

            $result = implode(", ", $designersArray);
        }

        return $result;
    }

    public function scopeReadyToEmailed($query)
    {
        $now = now()->format("Y-m-d H:i:s");

        return $query
            ->whereRaw("TIMESTAMPDIFF(HOUR, submited_date, '{$now}') >= 1")
            ->whereSubmitedEmail(0);
    }
}
