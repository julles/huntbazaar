<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use App\Services\Helper;

class ValidateInvitationUrl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $key = request()->segment(3);
        $encrypted = Helper::encryption(request()->segment(2));
        if ($encrypted != $key) {
            abort(404);
        }

        $dueDate = Carbon::parse("2020-11-20");

        if (now()->gte($dueDate)) {
            abort(404);
        }

        return $next($request);
    }
}
