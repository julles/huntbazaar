<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;

class InvitationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required",
            "date_of_birth" => "required|date",
            "gender" => "required|max:1",
            "designers.0" => "required",
        ];
    }

    public function messages()
    {
        return [
            "designers.*.required" => "The designers is required",
        ];
    }
}
