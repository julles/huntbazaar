<?php

namespace App\Http\Controllers\Admin;

use App\Models\Invitation;
use Illuminate\Http\Request;
use App\Jobs\SendInvitationJob;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Admin\Invitation\CreateRequest;

class InvitationController extends AdminController
{
    public function __construct(Invitation $model)
    {
        parent::__construct();

        $this->view .= "invitation.";
        $this->route .= "invitation";
        $this->model = $model;
        view()->share("route", $this->route);
    }

    public function getData(Request $request)
    {
        $model = $this->model->select('invitations.id', 'email', 'status')
            ->join("invitation_statuses", "invitation_statuses.id", "=", "invitations.invitation_status_id");

        return \Table::of($model)
            ->addColumn('action', function ($model) {
                return  \Html::link($this->route . "/detail/" . $model->id, "Detail", ["class" => "btn btn-default btn-sm"]);
            })
            ->make();
    }

    public function getIndex()
    {
        return $this->makeView("index");
    }

    public function getCreate()
    {
        return $this->makeView("form", [
            "model" => $this->model,
        ]);
    }

    public function postCreate(CreateRequest $request)
    {
        DB::beginTransaction();

        try {
            $invitation = Invitation::create([
                "invitation_status_id" => 1,
                "email" => $request->email,
            ]);

            dispatch(new SendInvitationJob($invitation));
            toast("The data has been saved, an email invitation has been sent!", "success");
            DB::commit();
        } catch (\Exception $e) {
            toast("whoops something went wrong!", "error");
            DB::rollback();
        }

        return redirect($this->route);
    }

    public function getDetail($id)
    {
        $model = $this->model->findOrFail($id);

        return $this->makeView("detail", [
            "model" => $model,
        ]);
    }
}
