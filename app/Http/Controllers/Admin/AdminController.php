<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    protected $view;

    public function __construct()
    {
        $this->view = "admin.";
        $this->route = "admin/";
    }

    public function makeView($view, $data = [])
    {
        return view($this->view . $view, $data);
    }
}
