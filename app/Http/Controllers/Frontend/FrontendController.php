<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

class FrontendController extends Controller
{
    protected $view;

    public function __construct()
    {
        $this->view = "frontend.";
    }

    public function makeView($view, $data = [])
    {
        return view($this->view . $view, $data);
    }
}
