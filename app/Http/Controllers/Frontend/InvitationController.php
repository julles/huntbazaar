<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Invitation;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Services\GrabDesigner;
use App\Http\Requests\Frontend\InvitationRequest;

class InvitationController extends FrontendController
{
    public function __construct(Invitation $model, GrabDesigner $grabDesigner)
    {
        parent::__construct();

        $this->model = $model;

        $this->grabDesigner = $grabDesigner;
    }

    public function getIndex(Request $request, $id, $key)
    {
        $grabDesigners = $this->grabDesigner->getData();

        $model = $this->model->findOrFail($id);

        return $this->makeView("invitation.index", [
            "model" => $model,
            "designers" => $grabDesigners,
        ]);
    }

    public function postIndex(InvitationRequest $request, $id, $key)
    {
        $inputs = $request->except("email");
        $inputs["designers"] = json_encode($inputs["designers"]);
        $inputs["code"] = Str::random(10);
        $inputs["submited_date"] = now()->format("Y-m-d H:i:s");


        $model = $this->model->findOrFail($id);
        $model->update($inputs);

        toast("Register successfully", "success");

        return back();
    }
}
