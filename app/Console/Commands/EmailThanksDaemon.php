<?php

namespace App\Console\Commands;

use App\Jobs\ThanksJob;
use App\Models\Invitation;
use Illuminate\Console\Command;

class EmailThanksDaemon extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email-thanks-daemon';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Listen all Submited Date invitation thats been more than 1 hour';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $model = Invitation::readyToEmailed()->get();

        foreach ($model as $row) {
            $row->update([
                "submited_email" => 1,
            ]);
            dispatch(new ThanksJob($row));
            $this->line($row->email . " Proccessed!");
        }
    }
}
