<?php
Route::view("/", "welcome");

Auth::routes();

Route::prefix("admin")->namespace("Admin")->middleware(["auth"])->group(function () {

    Route::prefix("invitation")->group(function () {
        Route::get("data", "InvitationController@getData");
        Route::get("create", "InvitationController@getCreate");
        Route::post("create", "InvitationController@postCreate");
        Route::get("detail/{id}", "InvitationController@getDetail");
        Route::get("/", "InvitationController@getIndex");
    });
});

Route::namespace("Frontend")->group(function () {
    Route::prefix("invitation")->middleware("validate-invitation-url")->group(function () {
        Route::get("/{id}/{key}", "InvitationController@getIndex");
        Route::post("/{id}/{key}", "InvitationController@postIndex");
    });
});
