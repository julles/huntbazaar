@component('mail::message')
# Hello {{ $invitation->email }}

Silahkan klik tombol dibawah ini untuk pergi kehalaman invitation

@component('mail::button', ['url' => url($invitation->url)])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
