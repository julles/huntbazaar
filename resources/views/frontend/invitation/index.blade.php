@extends("frontend.layouts.layout")
@section("content")
<section class="page-section" id="contact" style = "margin-top:40px;">
    <div class="container">
        @if($model->invitation_status_id == 2)
            @include("frontend.invitation.registered")
        @else
            @include("frontend.invitation.not_registered")
        @endif
    </div>
</section>
@endsection



