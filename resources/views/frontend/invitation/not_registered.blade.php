<h2 class="page-section-heading text-center text-uppercase text-secondary mb-0" id = "countdown">

</h2>
<!-- Icon Divider-->
<div class="divider-custom">
    <div class="divider-custom-line"></div>
    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
    <div class="divider-custom-line"></div>
</div>


<div class="row">
    <div class="col-lg-8 mx-auto">
        @include("frontend.component.flash")
        {!! Form::model($model,["method" => "post","id" => "contactForm"]) !!}
            <div class="control-group">
                <div class="form-group controls mb-0 pb-2">
                    <label>Email</label>
                    {!! Form::text("email",null,["class" => "form-control","placeholder"=>"Name","readonly","style" => "background:white;"]) !!}
                </div>
            </div>
            <div class="control-group">
                <div class="form-group  controls mb-0 pb-2">
                    <label>Name</label>
                    {!! Form::text("name",null,["class" => "form-control","placeholder"=>"Name"]) !!}
                </div>
            </div>
            <div class="control-group">
                <div class="form-group controls mb-0 pb-2">
                    <label>Date of Birth</label>
                    {!! Form::text("date_of_birth",null,["class" => "form-control datepicker","placeholder"=>"Date of Birth","readonly","style" => "background:white;"]) !!}
                </div>
            </div>
            <div class="control-group">
                <div class="form-group controls mb-0 pb-2">
                    <label>Gender</label>
                    {!! Form::select("gender",["f"=>"Female","m"=>"Male"],null,["class" => "form-control"]) !!}
                </div>
            </div>
            <div class="control-group">
                <div class="form-group controls mb-0 pb-2">
                    <label>Designers</label>
                    {!! Form::select("designers[]",$designers,null,["class" => "form-control select2","multiple" => "multiple"]) !!}
                </div>
            </div>
            <br />
            <div id="success"></div>
            <div class="form-group"><button class="btn btn-primary btn-xl" id="sendMessageButton" type="submit">Register</button></div>
        {!! Form::close() !!}
    </div>
</div>

@push("js")
<script>
    $( function() {
        $(".datepicker").datepicker({
            changeMonth:true,
            changeYear:true,
            dateFormat:"yy-mm-dd",
        });

        $(".select2").select2();
    });
</script>

<script src="/js/countdown.js"></script>

@endpush