{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('content_header')
<h1>Invitation</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="box box-success">
            <div class="box-header">
                Create
            </div>
            {!! Form::model($model,["id" =>"form","method"=>"post"]) !!}
            <div class="box-body">
                <div class="form-group">
                    {!! Form::label("email") !!}
                    {!! Form::text("email",null,["class"=>"form-control"]) !!}
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                    Save
                </button>

                <a href="{{ url($route) }}" class="btn btn-default btn-sm">
                    Back
                </a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop

@push("js")
{!! JsValidator::formRequest('App\Http\Requests\Admin\Invitation\CreateRequest', '#form'); !!}
@endpush
