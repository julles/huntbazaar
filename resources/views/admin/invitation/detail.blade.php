{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('content_header')
<h1>Invitation</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="box box-success">
            <div class="box-header">
                Detail
            </div>
            {!! Form::model($model,["id" =>"form","method"=>"post"]) !!}
            <div class="box-body">
                <div class="form-group">
                    {!! Form::label("email") !!}
                    {!! Form::text("email",null,["class"=>"form-control","readonly"=>true]) !!}
                </div>
                <div class="form-group">
                    {!! Form::label("name") !!}
                    {!! Form::text("name",null,["class"=>"form-control","readonly"=>true]) !!}
                </div>
                <div class="form-group">
                    {!! Form::label("status") !!}
                    {!! Form::text("status",$model->status->status,["class"=>"form-control","readonly"=>true]) !!}
                </div>
                <div class="form-group">
                    {!! Form::label("date_of_birth") !!}
                    {!! Form::text("date_of_birth",null,["class"=>"form-control","readonly"=>true]) !!}
                </div>
                <div class="form-group">
                    {!! Form::label("gender") !!}
                    {!! Form::text("gender_text",null,["class"=>"form-control","readonly"=>true]) !!}
                </div>
                <div class="form-group">
                    {!! Form::label("code") !!}
                    {!! Form::text("code",null,["class"=>"form-control","readonly"=>true]) !!}
                </div>
                <div class="form-group">
                    {!! Form::label("designers") !!}
                    {!! Form::textarea("designers_text",null,["class"=>"form-control","readonly"=>true]) !!}
                </div>
                
            </div>
            <div class="box-footer">
                <a href="{{ url($route) }}" class="btn btn-default btn-sm">
                    Back
                </a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop
