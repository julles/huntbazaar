@extends('adminlte::page')

@section('content_header')
    <h1>List</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-default">
            <div class="box-header">
                {!! Html::link($route."/create","Add New",["class"=>"btn btn-primary btn-sm"]) !!}
            </div>
            <div class="box-body">
                <table class="table table-bordered" id = "table">
                    <thead>
                        <tr>
                            <th width = "40%">Email</th>
                            <th width = "40%">Status</th>
                            <th width = "20%">Action</th>
                        </tr>
                    </thead>
                    <tbody id = "tbody">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop

@push('js')
<script>
$(function() {
    var table = $('#table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ url($route.'/data') }}",
        columns: [
            { data: 'email', name: 'email' },
            { data: 'status', name: 'invitation_statuses.status' },
            { data: 'action', name: 'action' ,searchable:false}
        ]
    });
});
</script>
@endpush
